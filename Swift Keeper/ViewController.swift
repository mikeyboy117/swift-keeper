//
//  ViewController.swift
//  Swift Keeper
//
//  Created by Michael Childs on 1/25/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        player1name.text = "0";
        player2name.text = "0";
        
        func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
            self.view.endEditing(true);
        }
        
    }
    
    
    @IBOutlet var player1TextField: UITextField!
    @IBOutlet var player2TextField: UITextField!
    
    @IBOutlet var player1name: UILabel!

    @IBOutlet var player2name: UILabel!
    
    @IBOutlet var resetBtn: UIButton!
    
    @IBOutlet var player1Score: UILabel!
    
    @IBOutlet var player2Score: UILabel!
    
    @IBOutlet var player1Stepper: UIStepper!
    
    @IBOutlet var player2Stepper: UIStepper!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func player1StepperTouched(sender: UIStepper) {
        
        player1Score.text = NSString(format: "%.f", player1Stepper.value) as String
        
    }
    
    @IBAction func player2StepperTouched(sender: UIStepper) {
        
        player2Score.text = NSString(format: "%.f", player2Stepper.value) as String
        
    }
    

    @IBAction func resetBtnTouched(sender: UIButton) {
        player1Score.text = "0";
        player2Score.text = "0";
        
        player1Stepper.value = 0;
        player2Stepper.value = 0;
    }
    
    
//    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        self.view.endEditing(true);
//    }
    
}

